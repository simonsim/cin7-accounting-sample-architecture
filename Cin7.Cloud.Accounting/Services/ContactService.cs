﻿using Cin7.Cloud.Accounting.Exceptions;
using Cin7.Cloud.Accounting.Interfaces;
using Cin7.Cloud.Accounting.Models;
using Cin7.Cloud.Accounting.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cin7.Cloud.Accounting.Services
{
    public class ContactService
    {
        private readonly IExternalAccountingService _accountingService;
        private readonly IContactRepository _contactRepository;

        public ContactService(IExternalAccountingService accountingService,
            IContactRepository contactRepository)
        {
            _accountingService = accountingService;
            _contactRepository = contactRepository;
        }

        public void ImportAll()
        {
            // 1. Find contacts we need to import
            var query = new ContactQuery();
            var contacts = _contactRepository.GetContacts(query);

            try
            {
                // 2. Maybe do some extra operations
                PerformSomeOperation();

                // 3. Execute!
                _accountingService.ImportContacts(contacts);
            }
            catch
            {
                //Log to Sumo and Raygun
                throw;
            }
        }

        public void Import(Contact[] contacts)
        {

        }

        private void PerformSomeOperation()
        {
            //uh oh 
            throw new ContactServiceImportException();
        }
    }
}

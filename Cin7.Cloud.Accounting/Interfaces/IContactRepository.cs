﻿using Cin7.Cloud.Accounting.Models;
using Cin7.Cloud.Accounting.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cin7.Cloud.Accounting.Interfaces
{
    public interface IContactRepository
    {
        List<Contact> GetContacts(ContactQuery query);
    }
}

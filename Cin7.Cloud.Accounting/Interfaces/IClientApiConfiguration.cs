﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cin7.Cloud.Accounting.Interfaces
{
    public interface IClientApiConfiguration
    {
        void ConfigureApiKey(string apiKey, string secret);
    }
}

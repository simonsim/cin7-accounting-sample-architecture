﻿using Cin7.Cloud.Accounting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cin7.Cloud.Accounting.Interfaces
{
    public interface IExternalAccountingService
    {
        void ImportContacts(List<Contact> contacts);
    }
}

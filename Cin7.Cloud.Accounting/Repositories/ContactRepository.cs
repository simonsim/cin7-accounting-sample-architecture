﻿using Cin7.Cloud.Accounting.Interfaces;
using Cin7.Cloud.Accounting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cin7.Cloud.Accounting.Repositories
{
    public class ContactRepository : IContactRepository
    {
        public ContactRepository ()
        {
            // inject SqlHelper here
        }

        public List<Contact> GetContacts(ContactQuery query)
        {
            //db.ExecuteQuery(query)
            return new List<Contact>();
        }

        public List<Contact> GetNewContacts(ContactQuery query)
        {
            //db.ExecuteQuery(query)
            return new List<Contact>();
        }
    }
}

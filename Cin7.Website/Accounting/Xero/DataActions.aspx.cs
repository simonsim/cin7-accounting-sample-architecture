﻿using Cin7.Cloud.Accounting.Interfaces;
using Cin7.Cloud.Accounting.Repositories;
using Cin7.Cloud.Accounting.Services;
using System;

namespace Cin7.Website.AccountingDemo.Xero
{
    public partial class DataActions : System.Web.UI.Page
    {
        private ContactService _contactService;
        private IExternalAccountingService _xero;
        private IContactRepository _contactRepository;
        private IClientApiConfiguration _xeroApiConfiguration;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Setup dependencies, preferably using ninject
            _xero = new Cin7.Cloud.Accounting.Services.Xero(_xeroApiConfiguration);
            _contactRepository = new ContactRepository();
            _contactService = new ContactService(_xero, _contactRepository);

        }

        protected void ImportContactsButton_Click(object sender, EventArgs e)
        {
            var contacts = _contactRepository.GetContacts(new ContactQuery());
            _contactService.Import(contacts.ToArray());
        }
    }
}
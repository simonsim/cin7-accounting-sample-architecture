﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataActions.aspx.cs" Inherits="Cin7.Website.AccountingDemo.QuickBooks.DataActions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button ID="ImportContactsButton" runat="server" Text="Import Contacts into QuickBooks" OnClick="ImportContactsButton_Click" />
        </div>
    </form>
</body>
</html>

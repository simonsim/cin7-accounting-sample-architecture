﻿using Cin7.Cloud.Accounting.Interfaces;
using Cin7.Cloud.Accounting.Repositories;
using Cin7.Cloud.Accounting.Services;
using System;

namespace Cin7.Website.AccountingDemo.QuickBooks
{
    public partial class DataActions : System.Web.UI.Page
    {
        private ContactService _contactService;
        private IExternalAccountingService _quickBooks;
        private IContactRepository _contactRepository;
        private IClientApiConfiguration _quickBooksApiConfiguration;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Setup dependencies, preferably using ninject
            _quickBooks = new Cin7.Cloud.Accounting.Services.QuickBooks(_quickBooksApiConfiguration);
            _contactRepository = new ContactRepository();
            _contactService = new ContactService(_quickBooks, _contactRepository);
        }

        protected void ImportContactsButton_Click(object sender, EventArgs e)
        {
            var query = new ContactQuery();
            var contacts = _contactRepository.GetContacts(query);
            _quickBooks.ImportContacts(contacts);
        }
    }
}